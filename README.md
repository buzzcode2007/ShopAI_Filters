# ShopAI Filters

**_Let AI read that e-commerce page!_** 

This repository contains templates of filters to scrape website data from e-commerce webpages, particularly data within the current tab. While typically made for ShopAI, this may be used by compatible software; to do so, kindly refer to the wiki, the templates, and the examples. 
